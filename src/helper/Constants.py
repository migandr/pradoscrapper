# PATHS
# Used  for files navigation and files open
# READER = 'C:\\Users\\miguel.nieva\\Desktop\\gitLab_migandr\\python-functionalities\\helperForPrado\\src\\read\\'
# EXPORT = 'C:\\Users\\miguel.nieva\\Desktop\\gitLab_migandr\\python-functionalities\\helperForPrado\\src\\export\\'
# THREE_HOURS = 'C:\\Users\\miguel.nieva\\Desktop\\gitLab_migandr\\python-functionalities\\helperForPrado\\src\\read\\recorrido3horas.json'
# ALL_ARTWORKS = 'C:\\Users\\miguel.nieva\\Desktop\\gitLab_migandr\\python-functionalities\\helperForPrado\\src\\export\\PieceOfArt3JSON.json'
READER = "read/"
EXPORT = "export/"
THREE_HOURS = "read/recorrido3horas.json"
ALL_ARTWORKS = "export/PieceOfArt3JSON.json"
AUTHOR_EXPORT_NAME = 'author.js'

# EXTENSIONS & FILES
# Used during the files creation
JSON_EXT = '.json'
JS_EXT = '.js'
VAR_AUTHOR = 'var author = '
VAR_PIECE_OF_ART = 'var pieceOfArt = '

# KEYS
# Used to start reading JSON files
READER_CHECK_VALUE = 'reader'
ARTWORK_TAG = 'reco'
ARTWORK = 'obras'
AW_KEY = 'aw'
AU_KEY = 'au'

# NORMALIZATION
# Tags to be removed during the normalization of the descriptions
HTML_TAGS = ['<p>', '</p>',
             '<em>', '</em>',
             '<strong>', '</strong>',
             '<i>', '</i>',
             '<mark>', '</mark>',
             '<small>', '</small>',
             '<del>', '</del>',
             '<ins>', '</ins>',
             '<sub>', '</sub>',
             '<sup>', '</sup>']

# TITLES
# Text messages during the process
TITLE_BEGIN_AW = 'BEGIN ART WORKS'
TITLE_BEGIN_AU = 'BEGIN AUTHOR'
TITLE_WIP = 'WORKING ON {0} ELEMENTS'
TITLE_WRITING = 'WRITING FILE {0}'
TITLE_IN_PROGRESS = 'In progress {0} of {1}'
SUCCESS_MESSAGE = 'PROCESS SUCCESFULLY CONCLUDED, PLEASE CHECK "export" FOLDER'

# FORMATS
# Give required format to JSON fields
AW_SIZE_FORMAT = '{0} x {1} cm.'

# FILE NAMES
# Helper to recognize file names
R1 = 'recorrido1hora'
R2 = 'recorrido2horas'
R3 = 'recorrido3horas'
P1 = 'PieceOfArt1JSON'
P2 = 'PieceOfArt2JSON'
P3 = 'PieceOfArt3JSON'

ERROR_WHILE_CREATING_ARTWORKS = 'During the creation of the artworks jsons something went wrong'
