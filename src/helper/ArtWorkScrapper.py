#! usr/bin/Python3.7
import requests
import re
from bs4 import BeautifulSoup


def return_localization_image(art_work_url):
    """
    BS4 implementation to find an image for the "localization in the museum" image
    :param art_work_url: the artwork address
    :return: a permalink for the image
    """
    file_name = art_work_url
    r = requests.get(file_name)
    soup = BeautifulSoup(r.content, features='html.parser')
    myimg = soup.findAll('img', {'class': 'mostrable oculto'})
    try:
        img_code = str(myimg[0])
        img_code = img_code.split(' ')
        img_to_return = img_code[len(img_code)-1]
        img_to_return = img_to_return.split('"')
        return img_to_return[1]
    except (BaseException, Exception):
        return ""


def return_author_born_and_dead(art_work_url):
    """
    BS4 implementation to find the born and death dates for an author in an artwork
    :param art_work_url: the artwork address
    :return: a string with the information
    """
    file_name = art_work_url
    r = requests.get(file_name)
    soup = BeautifulSoup(r.content, features='html.parser')
    extrainfo = soup.find('div', {'class': 'autor'})
    if bool(extrainfo) is False:
        return '-'
    prog = re.compile(r'(?<=\<h2>)(.*)(?=\<\/h2\>)')
    for e in extrainfo:
        if prog.search(str(e)):
            e = str(e)
            e = e.replace('<h2>', '')
            e = e.replace('</h2>', '')
            return e
    return '-'
