#! usr/bin/Python3.7
from helper import ArtWorkScrapper as AWS
from helper import Constants as Con


class JsonArtWorks:

    def __init__(self, artwork):
        """
        Init for a json of artwork
        :param artwork: a line of daya from the base json
        """
        if bool(artwork['ubicacion']['localizacion']):
            ubicacion = artwork['ubicacion']['localizacion']
        else:
            ubicacion = '-'
        self.__constructor__(artwork['id'], artwork['autores'][0], artwork['titulo'][0]['valor'],
                             artwork['fecha'][0]['valor'], artwork['descripcion'][0]['valor'], artwork['imagen'],
                             artwork['dimensiones'], artwork['soportes'], artwork['tecnicas'], ubicacion)

    def __constructor__(self, piece_of_art_id, author, piece_of_art_title, date_of_creation, piece_description,
                        img_permalink, size, support, piece_technic, localization):
        """
        Constructor for an artwork object
        :param piece_of_art_id: unique identifier
        :param author: information of the author
        :param piece_of_art_title: title of the artwork
        :param date_of_creation: date of creation of the artwork
        :param piece_description: a description to be normalized
        :param img_permalink: the permalink for the artwork image
        :param size: the size of the arwork image
        :param support: the support where the artwork has been created
        :param piece_technic: the artwork creation technique
        :param localization: the permalink for the image lcalization
        """
        self.pieceOfArtId = self.id_extractor(piece_of_art_id)
        self.piecePermalink = piece_of_art_id
        self.authorPermalink = author['id']
        self.authorName = author['nombre'][0]['valor']
        self.bornDeath = AWS.return_author_born_and_dead(self.piecePermalink)
        self.pieceTourId = 0
        self.positionInTheTour = 0
        self.pieceOfArtTitle = piece_of_art_title
        self.dateOfCreation = date_of_creation
        self.pieceDescription = self.description_extractor(piece_description)
        self.imgPermalink = img_permalink
        self.size = self.size_formatter(size)
        self.support = support[0]['nombre'][0]['valor'] if bool(support) else '-'
        self.pieceTechnic = piece_technic[0]['nombre'][0]['valor'] if bool(piece_technic) else "-"
        self.localizationPermalink = AWS.return_localization_image(piece_of_art_id)
        self.localization = localization

    @staticmethod
    def id_extractor(piece_of_art_id):
        """
        Normalizer for pieceOfArtId
        :param piece_of_art_id: not normalized format
        :return: normalized format
        """
        piece_of_art_id = piece_of_art_id.split('/')
        return piece_of_art_id[len(piece_of_art_id)-1]

    @staticmethod
    def description_extractor(piece_description):
        """
        Prepares the description paragraphs aligned with BIXBY requirements
        :param piece_description: data raw
        :return: normalized description
        """
        piece_description = piece_description.split('</p><p>')
        descriptions = {}
        for i, description in enumerate(piece_description):
            for tag in Con.HTML_TAGS:
                description = description.replace(tag, '')
            if len(description) > 500:
                descriptions[i] = description[:500] + '...'
                return descriptions
            else:
                descriptions[i] = description
            if i is 1:
                descriptions[i] = description[:500] + '...'
        return descriptions

    @staticmethod
    def size_formatter(size):
        """
        Prepares in the capsule expected format, the size of the artwork
        :param size: data raw
        :return: string with the format
        """
        height = size[0]['nombre'][0]['valor'].split(',')[0]
        height = height.split(' ')[1]
        width = size[1]['nombre'][0]['valor'].split(',')[0]
        width = width.split(' ')[1]
        return Con.AW_SIZE_FORMAT.format(str(height), str(width))
