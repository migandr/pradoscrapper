import os
from helper import Constants as Con
import json


def move_back():
    """
    Find one folder back in the folder tree
    :return: one step back folder address
    """
    source_dir = os.getcwd()
    dir_split = source_dir.split('/')
    ret_dir = ''
    for i, level in enumerate(dir_split):
        if i < len(dir_split) - 1:
            ret_dir = ret_dir + level + "/"
    return ret_dir


def change_to(destination):
    """
    Move pointer to a specific folder
    :param destination: a normalized path
    :return: None
    """
    if destination is Con.READER_CHECK_VALUE:
        os.chdir(Con.READER)


def find_files(source, goal):
    """
    Check in a specific folder if the requested files exists.
    :param source: Where to find
    :param goal: What pattern must be find
    :return: a list of files
    """
    list_of_files = []
    for file in os.listdir(source):
        if file[:4] == goal:
            list_of_files.append(os.getcwd() + '/' + file)
    return list_of_files


def build_file(pure_object, file):
    """
    Build a file with a specific data
    :param pure_object: data to be written
    :param file: location + file
    :return: None
    """
    json_object = json.dumps([ob.__dict__ for ob in pure_object],
                             sort_keys=True, indent=4, ensure_ascii=False)
    if file is not Con.AUTHOR_EXPORT_NAME:
        file_name = file.split("/")
        file_name = file_name[len(file_name) - 1].split('.')
        if file_name[0] == Con.R3:
            prepare_json_file(json_object, file_name)
        file_name = name_selector(file_name[0]) + Con.JS_EXT
        json_object = Con.VAR_PIECE_OF_ART + json_object
    else:
        file_name = file
        json_object = Con.VAR_AUTHOR + json_object
    try:
        os.mkdir(os.getcwd() + "/" + Con.EXPORT)
    except (BaseException, Exception):
        pass
    file_name = Con.EXPORT + file_name
    data_file = open(os.getcwd() + "/" + file_name, 'wb')
    data_file.write(json_object.encode('utf-8'))
    data_file.close()


def prepare_json_file(json_object, file_name):
    """
    Prepares the JSON file required for operate with Authors in the next step
    :param json_object: data to be written
    :param file_name: location + file
    :return:
    """
    file_name = name_selector(file_name[0]) + Con.JSON_EXT
    file_name = Con.EXPORT + file_name
    try:
        print("mkdir")
        os.mkdir(os.getcwd() + "/" + Con.EXPORT)
    except (Exception, BaseException):
        pass
    data_file = open(os.getcwd() + "/" + file_name, 'wb')
    data_file.write(json_object.encode('utf-8'))
    data_file.close()


def name_selector(name):
    """
    Switch local method to normalize file names
    :param name: origin file name
    :return: target file name
    """
    switcher = {
        Con.R1: Con.P1,
        Con.R2: Con.P2,
        Con.R3: Con.P3
    }
    return switcher.get(name, 'Error')


def data_set_extractor(file):
    """
    Opens a JSON file and return the required data tree
    :param file: which file must be open
    :return: JSON data
    """
    art_works = json.load(open(file, 'r', encoding='utf-8'))
    return art_works[Con.ARTWORK]
