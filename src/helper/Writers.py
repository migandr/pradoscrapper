from helper import Constants as Con


def write_begin(begin_element):
    """
    Prints starting with a task message
    :param begin_element: which tasks is beginning
    """
    print(Con.TITLE_BEGIN_AW) if begin_element is 'aw' else print(Con.TITLE_BEGIN_AU)


def write_count(leng, count):
    """
    Prints an * for each line handled
    """
    print(Con.TITLE_IN_PROGRESS.format(count, leng), end='\r')
    return count + 1


def write_file(file_name):
    """
    Prints writting file notification
    """
    print('')
    print(Con.TITLE_WRITING.format(file_name))


def write_wip(wip_element):
    """
    Prints how many task are in progress
    :param wip_element: which main task is in progress
    """
    print(Con.TITLE_WIP.format(wip_element))
