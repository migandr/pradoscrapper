#! usr/bin/Python3.7


def __list_of_authors__(art_works):
    """
    Prepares the list of authors to be scrapped
    :param art_works: All the existing artworks in BIXBY capsule
    :return: a list of authors
    """
    temp_author_list = []
    for art_work in art_works:
        author = art_work['autores'][0]
        if not author['id'] in temp_author_list:
            if author['nombre'][0]['valor'] != 'Anónimo':
                temp_author_list.append(author['id'])
    return temp_author_list
