# PRADO HELPER TOOL


This tool gives the expected format to the artWorks `json` data model
and build the authors `json` data model by scrapping the **Museo del
 Prado** website.
 
 The tool returns as many `js` files as required with the data and the structure
 to be used as `data_set` in any application.
 
 
 ### Prerequistes
 
**Language: `Python`** v: 3.7

**Returns: `Javascript`**
 
 Requires the installation of BeautifulSoup4
 
 `pip install bs4`

Other more common libraries used are:

`re`, `os`, `requests` and `json`
 
 
 ### Main explanation
 
 0- Files to be readed must be allocated in `/read` folder.
 
 1- Artworks json creator:
Using `json` provided by the CP Museo del Prado find and extract
the necessary fields to provide data to the `BIXBY Capsule` 
application for them.
  
2- Author json creator:
After artworks extraction, extract and discriminate the authors, scraps Prado
website in order to populate required data, checks all the artworks existing
in the `data_set` and populate his artworks in order to have all the author
information and his artworks in an unique object.

3- Returned files will be allocated in the `/export` folder.


_For in detail explanation, please see docstrings for each Method._
 
