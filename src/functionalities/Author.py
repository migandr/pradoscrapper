#! usr/bin/Python3.7
import json
import requests
from bs4 import BeautifulSoup
from helper import Constants as Con
import os


class JsonAuthor:

    def __init__(self, author_url):
        """
        Init for Json Author
        :param author_url: the author URL to be scrapped
        """
        file_name = author_url
        r = requests.get(file_name)
        soup = BeautifulSoup(r.content, features='html.parser')
        self.__constructor__(author_url, soup)

    def __constructor__(self, author_url, website):
        """
        Constructor for an author object
        :param author_url: the url where some data can be extracted
        :param website: the opened website structure to be scrapped
        """
        self.authorId = self.id_extractor(author_url)
        self.authorPermalink = author_url
        self.authorImgPermalink = self.get_author_image(website)
        self.authorName = self.author_name(website)
        self.bornDeath = self.author_born_death(website)
        self.school = self.get_school(website)
        self.authorDescription = self.get_clean_description(website)
        self.AuthorPieceOfArt = self.prepare_art_works()

    @staticmethod
    def id_extractor(author_url):
        """
        Extract and format de author ID
        :param author_url: data raw
        :return: formatted author ID
        """
        author_id = author_url.split('/')
        return author_id[len(author_id)-1]

    @staticmethod
    def author_name(website):
        """
        Extract the author name
        :param website: data raw
        :return: formatted author name
        """
        clear_article = website.article
        return clear_article.h1.get_text()

    @staticmethod
    def author_born_death(website):
        """
        Extract the information about date of born and death
        :param website: data raw
        :return: formatted born and death date or '-' if is not possible to solve
        """
        clear_article = website.article
        if bool(clear_article.strong):
            return clear_article.strong.get_text()
        else:
            return '-'

    @staticmethod
    def get_clean_description(website):
        """
        Extract a normalized description based on BIXBY rules
        :param website: data raw
        :return: formatted description or '-' if is not possible to solve
        """
        clear_article = website.article
        if clear_article.div.p.get_text() is not '':
            return clear_article.div.p.get_text()[:500] + '...'
        else:
            return '-'

    @staticmethod
    def get_school(website):
        """
        Extract the author's school
        :param website: data raw
        :return: formatted author's school or '-' if is not possible to solve
        """
        side_der = website.find('div', 'tags')
        spans = side_der.find_all('span')
        if len(spans) > 1:
            return spans[len(spans) - 1].a.get_text()
        else:
            return '-'

    @staticmethod
    def get_author_image(website):
        """
        Findes a representative image for the author if exists
        :param website: data raw
        :return: a permalink for the image
        """
        header = website.find('section', 'imagen-cabecera')
        if bool(header):
            return header['data-img']
        else:
            all_art_works = website.find('div', 'imgwrap')
            return all_art_works.a.img['src']

    def prepare_art_works(self):
        """
        Return an object with the basic information to display the author artworks that appears in BIXBY
        :return: list of artworks
        """
        art_works_list = json.load(open(os.getcwd() + "/" + Con.ALL_ARTWORKS, 'r', encoding='utf-8'))
        pieces_of_art = []
        for art_work in art_works_list:
            if str(self.authorName) == str(art_work['authorName']):
                piece_of_art = {}
                piece_of_art['piecePermalink'] = art_work['piecePermalink']
                piece_of_art['imgPermalink'] = art_work['imgPermalink']
                piece_of_art['pieceOfArtTitle'] = art_work['pieceOfArtTitle']
                piece_of_art['dateOfCreation'] = art_work['dateOfCreation']
                pieces_of_art.append(piece_of_art)
        return pieces_of_art


