#! usr/bin/Python3.7
from helper import FilesHandler as Fh
from helper import Constants as Con
from helper import Writers as Wri
from helper import MainHelpers as Mh
from functionalities import ArtWorks as Aw
from functionalities import Author as Au
import os


def __main__():
    """
    Initializator for the program
    """
    if __launch_json_adapter__() is 200:
        if __launch_author_adapter__() is 200:
            print(Con.SUCCESS_MESSAGE)
    else:
        print(Con.ERROR_WHILE_CREATING_ARTWORKS)


def __launch_json_adapter__():
    """
    Launches the proccedures to create the artwork json
    :return: 200 as confirmation of success
    """
    Wri.write_begin(Con.AW_KEY)
    list_of_files = Fh.find_files(Fh.change_to(Con.READER_CHECK_VALUE), Con.ARTWORK_TAG)
    for file in list_of_files:
        art_works = Fh.data_set_extractor(file)
        list_of_art_works = []
        Wri.write_wip(len(art_works))
        count = 0
        for art_work in art_works:
            count = Wri.write_count(len(art_works), count)
            list_of_art_works.append(__json_factory__(Con.AW_KEY, art_work))
        Wri.write_file(file)
        Fh.build_file(list_of_art_works, file)
    return 200


def __launch_author_adapter__():
    """
    Launches the proccedures to create the authors json
    :return: 200 as confirmation of success
    """
    Wri.write_begin(Con.AU_KEY)
    temp_path = os.getcwd()
    temp_path = temp_path.replace("/read", "")
    art_works = Fh.data_set_extractor(temp_path + "/" + Con.THREE_HOURS)
    author_list = Mh.__list_of_authors__(art_works)
    json_author_list = []
    Wri.write_wip(len(author_list))
    count = 0
    for author in author_list:
        count = Wri.write_count(len(author_list), count)
        json_author_list.append(__json_factory__(Con.AU_KEY, str(author)))
    Wri.write_file(Con.EXPORT + Con.AUTHOR_EXPORT_NAME)
    Fh.build_file(json_author_list, Con.AUTHOR_EXPORT_NAME)
    return 200


def __json_factory__(type_key, element):
    """
    Factory to build author or artwork object
    :param type_key: the key to decide which type of class must be called
    :param element: the element to create the object
    :return:
    """
    if type_key is Con.AW_KEY:
        new_element = Aw.JsonArtWorks(element)
    elif type_key is Con.AU_KEY:
        new_element = Au.JsonAuthor(element)
    return new_element


if __name__ == '__main__':
    __main__()
